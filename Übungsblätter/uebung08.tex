\newcommand{\sprache}{de}

\documentclass{uebung}

\begin{document}

\maketitle{8}

\begin{aufgabe}{Eigenschaften von Monomorphismen in $\widehat{A}$}
Beiweise folgende Aussagen über $\widehat{A}$.
\begin{teilaufgaben}
\item
Für jedes kommutative Diagramm
\[ \begin{tikzcd}
  X_1 \ar{d}{i_1} &
  X_0 \ar{d}{i_0} \ar{l}[swap]{x_1} \ar{r}{x_2} & 
  X_2 \ar[hook]{d}{i_2} \\
  S_1 &
  S_0 \ar{l}[swap]{s_1} \ar{r}{s_2} &
  S_2
\end{tikzcd} \]
in dem $i_2$
und $X_1 \amalg_{X_0} S_0 \hookrightarrow S_1$
Monomorphismen sind,
ist auch der induzierte Morphismus
$X_1 \amalg_{X_0} X_2 \to S_1 \amalg_{S_0} S_2$
ein Monomorphismus.
\item
Für jedes kommutative Diagramm
\[ \begin{tikzcd}
  X_1 \ar[hook]{d}{i_1} &
  X_0 \ar[hook]{d}{i_0} \ar[hook]{l}[swap]{x_1} \ar{r}{x_2} & 
  X_2 \ar[hook]{d}{i_2} \\
  S_1 &
  S_0 \ar[hook]{l}[swap]{s_1} \ar{r}{s_2} &
  S_2 \\
  Y_1 \ar[hook]{u}[swap]{j_1} &
  Y_0 \ar[hook]{u}[swap]{j_0} \ar[hook]{l}[swap]{y_1} \ar{r}{y_2} &
  Y_2 \ar[hook]{u}[swap]{j_2}
\end{tikzcd} \]
in dem die vertikalen Pfeile sowie $x_1$, $s_1$ und $y_1$
Monomorphismen sind
und außerdem die beiden linken Quadrate kartesisch (Pullbacks) sind,
ist der induzierte Morphismus
$(X_1 \times_{S_1} Y_1) \amalg_{(X_0 \times_{S_0} Y_0)}
(X_2 \times_{S_2} Y_2)
\to
(X_1 \amalg_{X_0} X_2) \times_{(S_1 \amalg_{S_0} S_2)}
(Y_1 \amalg_{Y_0} Y_2)$
ein Isomorphismus.
\item
Für jede kleine Familie
von Diagrammen $X_i \to S_i \leftarrow Y_i$
ist der induzierte Morphismus
$ \coprod_i (X_i \times_{S_i} Y_i) \to
(\coprod_i X_i) \times_{(\coprod_i S_i)} (\coprod_i Y_i)$
ein Isomorphismus.
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Eigenschaften von $L$ und $I$}
Zeige für die Funktoren $L : \widehat{A} \to \widehat{A}$
(die funktorielle Faktorisieung $X \to L(X) \to e$
wie im kleines-Objekt-Argument)
und $I : \widehat{A} \to \widehat{A}$
(den exakten Zylinder):
\begin{teilaufgaben}
\item
Sowohl $L$ als auch $I$ erhalten Monomorphismen.
\item
Sowohl $L$ als auch $I$
erhalten den Schnitt $U_1 \cap U_2 = U_1 \times_X U_2$
von Unterprägarben $U_1, U_2 \subseteq X$.
\item
Es existiert eine reguläre Kardinalzahl $\alpha$,
sodass gilt:
\begin{enumerate}[label=(\roman*)]
\item
Für jede Prägarbe $X$
ist die Vereinigung von weniger als $\alpha$ vielen
$\alpha$-kleinen Unterobjekten
wieder $\alpha$-klein.
\item
Für $\alpha$-kleines $X$
sind auch $L(X)$ und $X \otimes I$ wieder $\alpha$-klein.
\item
Für jede Prägarbe $X$ gilt $L(X) = \bigcup_{V} L(V)$,
wobei $V$ alle $\alpha$-kleinen Unterobjekte von $X$ durchläuft.
\item
Sei $\beta$ die kleinste Ordinalzahl der Kardinalität $\alpha$.
Dann gilt für jede aufsteigende Familie $(V_\lambda)_{\lambda<\beta}$
von Unterobjekten $V_\lambda \subseteq X$,
dass $L(\bigcup_{\lambda<\beta} V_\lambda) =
\bigcup_{\lambda<\beta} L(V_\lambda)$.
\end{enumerate}
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{\enquote{Alle} Modellstrukturen kommen aus dem Nichts}
Wir wollen zeigen,
dass jede kofasernd erzeugte Modellstruktur auf $\widehat{A}$,
deren Kofaserungen gerade die Monomorphismen sind,
auf die in der Vorlesung besprochene Weise
aus einer Menge anodyner Erweiterungen gewonnen werden kann.

Sei also
$(W_0, \Cof = \prescript{\perp}{}(M^\perp), \Fib_0 = J^\perp)$
eine Modellstruktur auf $\widehat{A}$,
sodass $\Cof$ gerade die Menge aller Monomorphismen ist,
und wobei $M$ und $J$ kleine Mengen sind.
(Es ist $M$ also ein zelluläres Modell.)
Wir wählen als exakten Zylinder $X \mapsto X \times \Omega$
mit dem Unterobjektklassifizierer $\Omega$.
Dann können wir die kleinste Menge $\Omega$-anodyner Erweiterungen,
die $J$ enthält, bilden,
$\An \defeq \An_\Omega(J)
= \prescript{\perp}{}(\Lambda_\Omega(J, M)^\perp)$.
Diese homotopische Struktur
liefert eine neue Modellstruktur $(W_1, \Cof, \Fib_1)$.

\begin{teilaufgaben}
\item
Zeige, dass jede naive Faserung
eine Faserung der ursprünglichen Modellstruktur ist:
$\An^\perp \subseteq J^\perp$.
\item
\label{projektion-triviale-faserung}
Zeige, dass für jede Prägarbe $X$
die Projektion $X \times \Omega \to X$
eine triviale Faserung ($\in \Cof^\perp$) ist.
\item
Folgere, dass für jedes $X$
der Zylinder $X \times \Omega$
auch ein Zylinder im Sinne der ursprünglichen Modellstruktur ist.
\item
\label{schwache-aequivalenzen-inklusion}
Beweise damit $W_0 \subseteq W_1$.
\item
Benutze Teilaufgabe \ref{projektion-triviale-faserung} noch einmal,
um zu zeigen, dass $\Cof \cap W_0$
eine Menge $\Omega$-anodyner Erweiterungen ist.
\item
Folgere, dass auch jede Faserung der ursprünglichen Modellstruktur
eine naive Faserung ist: $J^\perp \subseteq \An^\perp$.
\item
Zeige damit auch die Umkehrung
von Teilaufgabe \ref{schwache-aequivalenzen-inklusion}:
$W_1 \subseteq W_0$.
\end{teilaufgaben}
\end{aufgabe}

\end{document}
