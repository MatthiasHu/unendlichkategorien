\newcommand{\sprache}{de}

\documentclass{uebung}

\begin{document}

\maketitle{4}

Sei $F \subseteq \Mor(C)$ abgeschlossen
unter Komposition mit Isomorphismen.
(Dies ist zum Beispiel der Fall,
wenn $F$ stabil unter Retrakten ist.)
Für eine kleine Ordinalzahl $\lambda > 0$
ist eine \emph{$\lambda$-Sequenz von Morphismen in $F$}
ein kleiner Funktor $X : \lambda \to C$,
sodass für alle Ordinalzahlen $0 < \mu < \lambda$
der Kolimes $\colim_{\alpha < \mu} X_\alpha$ existiert
und der induzierte Morphismus
$\colim_{\alpha < \mu} X_\alpha \to X_\mu$ in $F$ liegt.
Es heißt $F$ \emph{stabil unter transfiniter Komposition},
falls für jede kleine Ordinalzahl $\lambda > 0$
und jede $\lambda$-Sequenz von Morphismen in $F$
der Kolimes $\colim_{\alpha < \lambda} X_\alpha$ existiert
und der Morphismus $X_0 \to \colim_{\alpha < \lambda} X_\alpha$
wieder in $F$ liegt.

\vspace{0.5cm}


\begin{aufgabe}{Transfinite und finite Komposition}
% C klein-definierbar?
Sei $F \subseteq \Mor(C)$
abgeschlossen unter Komposition mit Isomorphismen
und stabil unter transfiniter Komposition.
Zeige, dass $F$ dann eine Unterkategorie ist,
die alle Objekte von $C$ enthält.
Das heißt, $F$ enthält alle Identitätsmorphismen
und für Morphismen $f: X \to Y$, $g : Y \to Z$
mit $f, g \in F$ gilt auch $g \circ f \in F$.
\end{aufgabe}

\begin{aufgabe}{Stabilität unter kleinen Summen}
Sei $C$ eine klein-definierbare
und kovollständige Kategorie.
Sei $F \subseteq \Mor(C)$ eine Menge von Morphismen,
die alle Identitäten enthält
und stabil unter Pushouts
sowie unter transfiniter Komposition ist.
Zeige, dass $F$ dann stabil unter kleinen Summen ist:
Für jede kleine Familie von Morphismen
$(f_i)_{i \in I}$, $f_i : X_i \to Y_i$ mit $f_i \in F$
liegt auch der induzierte Morphismus
$\coprod_{i \in I} f_i : \coprod_{i \in I} X_i
\to \coprod_{i \in I} Y_i$
in $F$.
\end{aufgabe}

\begin{aufgabe}
  {Durch Hochhebungseigenschaften gegebene Mengen von Morphismen}
Sei $C$ eine klein-definierbare, kovollständige Kategorie.
Zeige für Morphismenmengen $F, F' \subseteq \Mor(C)$:
\begin{teilaufgaben}
\item
Es gilt $F \subseteq F'^\perp$
genau dann, wenn $F' \subseteq \prescript{\perp}{}F$.
\item
Wenn $F \subseteq F'$,
dann ist $\prescript{\perp}{}F' \subseteq \prescript{\perp}{}F$
und $F'^\perp \subseteq F^\perp$.
\item
Es gilt
$F^\perp = (\prescript{\perp}{}(F^\perp))^\perp$
und $\prescript{\perp}{}F =
\prescript{\perp}{}((\prescript{\perp}{}F)^\perp)$.
\item
Es ist $\prescript{\perp}{}F$ saturiert
und $F^\perp$ kosaturiert
(das heißt saturiert als Menge von Morphismen in $\op{C}$).
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Hochhebungseigenschaften in $\Set$}
Sei $i : \varnothing \to \{*\}$ die leere Abbildung
in eine einelementige Menge.
\begin{teilaufgaben}
\item
Zeige, dass $\{i\}^\perp$ die Menge der
surjektiven kleinen Abbildungen ist.
\item
Zeige unter Verwendung des Auswahlaxioms,
dass $\prescript{\perp}{}(\{i\}^\perp)$ die Menge der
injektiven kleinen Abbildungen ist.
\item
Zeige, dass $\prescript{\perp}{}(\{i\}^\perp)$
die kleinste saturierte Teilmenge von $\Mor(\Set)$ ist,
die $i$ enthält.
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Starrheit schwacher Faktorisierungssysteme}
Sei $(A, B)$ ein schwaches Faktorisierungssystem.
Zeige $A = \prescript{\perp}{}B$ und $B = A^\perp$.

\textit{Tipp: Retraktlemma.}
\end{aufgabe}

\begin{aufgabe}{Kleines-Objekt-Argument}
Sei $C$ eine lokal-kleine, klein-definierbare Kategorie
mit kleinen Kolimiten.
Sei $I \subseteq \Mor(C)$ eine kleine Menge von Morphismen.
Es existiere eine kleine Kardinalzahl $\kappa$,
sodass für die Quelle $K$ eines jeden Morphismus $i : K \to L$ in $I$
der Funktor $\Hom(K, \hole) : C \to \Set$
alle $\lambda$-indizierten Kolimiten erhält,
wobei $\lambda$ die Kardinalzahl $\kappa$
als Ordinalzahl aufgefasst sei.
Wir wollen zeigen,
dass dann $(\prescript{\perp}{}(I^\perp), I^\perp)$
ein schwaches Faktorisierungssystem für $C$ ist.
(Dabei gehen wir nach Theorem 12.2.2
aus Emily Riehl, \textit{Categorical homotopy theory} vor.)
\begin{teilaufgaben}
\item
Mache dir klar, warum nur die Faktorisierungseigenschaft zu zeigen ist,
dass also die anderen beiden Bedingungen
an ein schwaches Faktorisierungssystem erfüllt sind.
\item
Sei $f : X_0 \to Y$ ein beliebiger Morphismus in $C$.
Konstruiere ein kommutatives Quadrat der folgenden Form.
\[ \begin{tikzcd}
  \coprod\limits_{j \in I}
    \coprod\limits_{(a, b) \in \mathrm{Sq}(j, f)} \dom j
    \ar{d} \ar{r} &
  X_0 \ar{d}{f} \\
  \coprod\limits_{j \in I}
    \coprod\limits_{(a, b) \in \mathrm{Sq}(j, f)} \cod j
    \ar{r} &
  Y
\end{tikzcd} \]
Dabei sei $\mathrm{Sq}(j, f)$ für einen Morphismus $j \in I$
die Menge der kommutativen Quadrate von $j$ nach $f$,
also $\mathrm{Sq}(j, f) \defeq
\{ (a : \dom j \to X_0, b : \cod j \to Y)
\;|\; f \circ a = b \circ j \}$.
Bilde den Pushout $X_1$ des linken und des oberen Morphismus
und erhalte so Morphismen $i_1 : X_0 \to X_1$, $p_1 : X_1 \to Y$
mit $f = p_1 \circ i_1$.
Erkläre, warum $i_1 \in \prescript{\perp}{}(I^\perp)$ gilt.
(Wir können leider noch nicht $p_1 \in I^\perp$ zeigen.)
\item
Wiederhole diesen Prozess mit $p_1$ anstelle von $f$,
um schließlich eine Folge von Morphismen
\[ X_0 \xrightarrow{i_1} X_1 \xrightarrow{i_2} X_2
\xrightarrow{i_3} \ldots \]
zu erhalten.
Wie sollten nun $X_\omega$
sowie Morphismen $i_\omega : X_0 \to X_\omega$
und $p_\omega : X_\omega \to Y$ definiert werden,
damit wieder $f = p_\omega \circ i_\omega$
und $i_\omega \in \prescript{\perp}{}(I^\perp)$ gilt?
\item
Zeige $p_\omega \in I^\perp$ unter der Annahme $\kappa = \omega$.

\textit{Tipp:
Nutze für $(j_0 : K \to L) \in I$
die Kompaktheit von $K$, um den Pfeil $K \to X_\omega$
über ein $X_n$ zu faktorisieren.
Finde dann einen passenden Morphismus $L \to X_{n+1}$.}
\item
Verallgemeinere dieses Vorgehen auf $\kappa > \omega$.
(Schlage dazu gegebenenfalls
das Stichwort \emph{transfinite Rekursion} nach.)
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Kleine Produkte mit großen Kolimiten vertauschen}
Seien $\kappa$ eine reguläre kleine Kardinalzahl,
$J$ eine kleine Menge mit $\abs{J} < \kappa$
und $\lambda$ die Kardinalzahl $\kappa$ als Ordinalzahl aufgefasst.
Zeige, dass dann der Funktor
$\Set^J \to \Set,\, (X_j)_{j \in J} \mapsto \prod_{j \in J} X_j$
alle $\lambda$-indizierten Kolimiten erhält.
Das heißt,
für jede Familie $(X_j)_{j \in J}$ von Funktoren
$X_j : \lambda \to \Set,\, \alpha \mapsto X_{j, \alpha}$
ist die kanonische Abbildung
\[ \colim_{\alpha < \lambda} \prod_{j \in J} X_{j, \alpha} \to
\prod_{j \in J} \colim_{\alpha < \lambda} X_{j, \alpha} \]
eine Bijektion.
\end{aufgabe}

\end{document}
